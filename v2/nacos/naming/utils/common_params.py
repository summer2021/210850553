class CommonParams:
    CODE = "code"

    SERVICE_NAME = "serviceName"

    CLUSTER_NAME = "clusterName"

    NAMESPACE_ID = "namespaceId"

    GROUP_NAME = "groupName"

    LIGHT_BEAT_ENABLED = "lightBeatEnabled"

    NAMING_REQUEST_TIMEOUT = "namingRequestTimeout"