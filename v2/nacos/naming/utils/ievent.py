from abc import ABCMeta


class Event(metaclass=ABCMeta):
    pass
