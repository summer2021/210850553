class PropertyKeyConstants:
    IS_USE_CLOUD_NAMESPACE_PARSING = "isUseCloudNamespaceParsing"

    IS_USE_ENDPOINT_PARSING_RULE = "isUseEndpointParsingRule"

    ENDPOINT = "endpoint"

    ENDPOINT_PORT = "endpointPort"

    NAMESPACE = "namespace"

    USERNAME = "username"

    PASSWORD = "password"

    ACCESS_KEY = "accessKey"

    SECRET_KEY = "secretKey"

    RAM_ROLE_NAME = "ramRoleName"

    SERVER_ADDR = "serverAddr"

    CONTEXT_PATH = "contextPath"

    CLUSTER_NAME = "clusterName"

    ENCODE = "encode"

    CONFIG_LONG_POLL_TIMEOUT = "configLongPollTimeout"

    CONFIG_RETRY_TIME = "configRetryTime"

    MAX_RETRY = "maxRetry"

    ENABLE_REMOTE_SYNC_CONFIG = "enableRemoteSyncConfig"

    NAMING_LOAD_CACHE_AT_START = "namingLoadCacheAtStart"

    NAMING_CACHE_REGISTRY_DIR = "namingCacheRegistryDir"

    NAMING_CLIENT_BEAT_THREAD_COUNT = "namingClientBeatThreadCount"

    NAMING_POLLING_THREAD_COUNT = "namingPollingThreadCount"

    NAMING_REQUEST_DOMAIN_RETRY_COUNT = "namingRequestDomainMaxRetryCount"

    NAMING_PUSH_EMPTY_PROTECTION = "namingPushEmptyProtection"

    PUSH_RECEIVER_UDP_PORT = "push.receiver.udp.port"

    class SystemEnv:
        ALIBABA_ALIWARE_ENDPOINT_PORT = "ALIBABA_ALIWARE_ENDPOINT_PORT"

        ALIBABA_ALIWARE_NAMESPACE = "ALIBABA_ALIWARE_NAMESPACE"

        ALIBABA_ALIWARE_ENDPOINT_URL = "ALIBABA_ALIWARE_ENDPOINT_URL"
