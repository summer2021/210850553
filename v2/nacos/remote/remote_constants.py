class RemoteConstants:
    LABEL_SOURCE = "source"

    LABEL_SOURCE_SDK = "sdk"

    LABEL_SOURCE_CLUSTER = "cluster"

    LABEL_MODULE = "module"

    LABEL_MODULE_CONFIG = "config"

    LABEL_MODULE_NAMING = "naming"
