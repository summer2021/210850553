class ConfigConstants:
    TENANT = "tenant"

    DATA_ID = "dataId"

    GROUP = "group"

    CONTENT = "content"

    CONFIG_TYPE = "configType"

    ENCRYPTED_DATA_KEY = "encryptedDataKey"

    TYPE = "type"
